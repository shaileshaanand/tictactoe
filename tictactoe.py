import os
import random
bmap={7:0,8:1,9:2,4:3,5:4,6:5,1:6,2:7,3:8}
def clearscr():
    os.system('clear')
def display_board():
    clearscr()
    print(f' {board[0]} | {board[1]} | {board[2]}')
    print('--- --- ---')
    print(f' {board[3]} | {board[4]} | {board[5]}')
    print('--- --- ---')
    print(f' {board[6]} | {board[7]} | {board[8]}')
def player_input():
    flag=True
    while flag:
        userinput=input('Player 1: Do you want to be X or O?').lower()
        if userinput=='x' or userinput=='o':
            player1=userinput
            flag=False
        else:
            print("please enter either 'x' or 'o'")
    return player1
def place_marker(marker,position):
    board[bmap[position]]=marker
def win_check(mark):
    return ((board[0]==board[1]==board[2]==mark) or (board[3]==board[4]==board[5]==mark) or (board[6]==board[7]==board[8]==mark) or (board[0]==board[3]==board[6]==mark) or (board[1]==board[4]==board[7]==mark) or (board[2]==board[5]==board[8]==mark) or (board[0]==board[4]==board[8]==mark) or (board[2]==board[4]==board[6]==mark))
def is_available(position):
    return board[bmap[position]]==' '
def is_board_full():
    for i in board:
        if i==' ':
            return False
    return True
def player_choice(player):
    while True:
        try:
            plc=int(input(f'Player {player}, Enter your choice(1-9)'))
        except ValueError:
            print('Player enter choice between 1-9')
            continue
        if 0<plc<10:
            return plc
        else:
            print('Player enter choice between 1-9')

def want_to_replay():
    return input("Would you like to restart?(y/n)? ").lower()=='y'
gameflag=True
while gameflag:
    clearscr()
    print('Welcome to Tic Tac Toe!\n')
    board = [' ',' ',' ',' ',' ',' ',' ',' ',' ',]
    # board = ['o','x','x','x','x','o','o','x','x',]
    player1=player_input()
    if player1=='x':
        player2='o'
    else:
        player2='x'
    if random.randint(1,2)==1:
        isplayer1=True
        message='Player 1 goes first.'
    else:
        isplayer1=False
        message='Player 2 goes first.'
    showmessage=True
    game_on=True
    while game_on:
        display_board()
        if showmessage:
            print(message)
            showmessage=False
        if isplayer1:
            inpos=player_choice(1)
            if is_available(inpos):
                place_marker(player1,inpos)
                isplayer1=False
            else:
                message=f'Position {inpos} is already taken'
                showmessage=True
        else:
            inpos=player_choice(2)
            if is_available(inpos):
                place_marker(player2,inpos)
                isplayer1=True
            else:
                message=f'Position {inpos} is already taken'
                showmessage=True
        if is_board_full():
            display_board()
            print('Match Draw')
            break
        if win_check(player1):
            display_board()
            print('Player 1 wins')
            break
        elif win_check(player2):
            display_board()
            print('Player 2 wins')
            break
    if not want_to_replay():
        gameflag=False
        clearscr()










